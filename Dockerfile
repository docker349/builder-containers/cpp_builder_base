FROM ubuntu

MAINTAINER Bolshevik <bolshevikov.igor@gmail.com>


RUN apt-get update &&  DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
    apt-utils               \
    gnupg                   \
    gzip                    \
    iproute2                \
    locales                 \
    sudo                    \
    tar                     \
    unzip                   \
    build-essential         \
    cmake                   \
    python3                 \
    python3-venv            \
 && apt-get clean           \
 && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8 && update-locale

#make a user
RUN adduser --disabled-password --gecos '' user && \
    usermod -aG sudo user && \
    echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
RUN sudo ln -fs /bin/bash /bin/sh

USER user
ENV HOME /home/user
ENV LANG en_US.UTF-8

WORKDIR ${HOME}

# Set locale conf
RUN echo 'export LANG=en_US.UTF-8' >> ~/.bashrc

ENTRYPOINT ["/bin/bash", "-l", "-c"]
